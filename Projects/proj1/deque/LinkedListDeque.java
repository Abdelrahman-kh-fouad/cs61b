package deque;


import java.util.Iterator;

public class LinkedListDeque<T> implements Deque<T>, Iterable<T> {

    private class Node<T> {
        public T item;
        public Node next;
        public Node prev;

        public Node() {
        }

        public Node(T item) {
            this.item = item;
        }

        @Override
        public String toString() {
            return item.toString();
        }
    }

    private class DLList<T> {
        private Node<T> sentFront = new Node<>();
        private Node<T> sentBack = new Node<>();
        public int size = 0;

        public DLList() {
            sentBack.next = null;
            sentBack.prev = sentFront;
            sentFront.prev = null;
            sentFront.next = sentBack;
        }

        public void addFirst(T item) {
            Node<T> newNode = new Node<>(item);
            Node nextNode = getFirst();
            newNode.prev = sentFront;
            newNode.next = nextNode;

            nextNode.prev = newNode;
            sentFront.next = newNode;
            size += 1;
        }

        public void addLast(T item) {
            Node<T> newNode = new Node<>(item);
            Node prevNode = getLast();
            newNode.prev = prevNode;
            newNode.next = sentBack;

            prevNode.next = newNode;
            sentBack.prev = newNode;
            size += 1;
        }

        public Node<T> getLast() {
            return sentBack.prev;
        }

        public Node<T> getFirst() {
            return sentFront.next;
        }

        private Node removeNode(Node<T> current) {
            if (current == sentBack || current == sentFront) {
                return null;
            }
            Node prev = current.prev;
            Node next = current.next;
            prev.next = next;
            next.prev = prev;
            return current;
        }

        public T removeFirst() {
            T result = null;
            if (size > 0) {
                result = (T) removeNode(sentFront.next).item;
                size -= 1;
            }
            return result;
        }

        public T removeLast() {
            T result = null;
            if (size > 0) {
                result = (T) removeNode(sentBack.prev).item;
                size -= 1;
            }
            return result;
        }


    }

    DLList<T> list = new DLList<>();

    public void addFirst(T item) {
        list.addFirst(item);
    }

    public void addLast(T item) {
        list.addLast(item);
    }

    public int size() {
        return list.size;
    }

    public void printDeque() {
        System.out.println(this.toString());
    }

    private String toStringg() {
        Node first = list.getFirst();
        StringBuilder sb = new StringBuilder();
        while (first != list.sentBack) {
            sb.append(first + " ");
            first = first.next;
        }
        return sb.toString();
    }

    public T removeFirst() {
        return list.removeFirst();
    }

    public T removeLast() {
        return list.removeLast();
    }

    public T get(int index) {
        if (list.size <= index) {
            return null;
        }
        Node first = list.getFirst();
        int cnt = 0;
        while (cnt < index) {
            first = first.next;
            cnt += 1;
        }
        return (T) first.item;
    }

    public T getRecursive(int index) {
        return getRecursive(index, this.list.getFirst());
    }

    private T getRecursive(int index, Node<T> current) {
        if (index == 0) {
            return current.item;
        }
        return (T) getRecursive(index - 1, current.next);
    }

    @Override
    public Iterator<T> iterator() {
        return new LLDequeIterator(this);
    }

    public boolean equals(Object o) {
        LinkedListDeque other = (LinkedListDeque<T>) o;
        return other.toStringg().equals(this.toStringg());
    }

    private class LLDequeIterator implements Iterator<T> {
        private int ind = 0;
        private LinkedListDeque<T> list;

        public LLDequeIterator(LinkedListDeque<T> list) {
            this.list = list;
        }

        @Override
        public boolean hasNext() {
            return ind + 1 < list.size();
        }

        @Override
        public T next() {
            if (!hasNext()) return null;
            return list.get(ind++ + 1);
        }
    }
}
