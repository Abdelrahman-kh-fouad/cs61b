package deque;

import java.util.Iterator;

public class ArrayDeque<T> implements Deque<T> {
    private T arr[] = (T[]) new Object[8];
    private int st = 0, en = -1;
    private int size = 0;

    private int mod(int n, int base) {
        return n - (int) Math.floor((double) n / base) * base;
    }

    private void resize(int newLength) {
        T a[] = (T[]) new Object[newLength];
        for (int i = st; i <= en; i++) {
            a[mod(i, newLength)] = arr[mod(i, arr.length)];
        }
        arr = a;
    }

    public void addFirst(T item) {
        if (size == arr.length) {
            resize(arr.length * 2);
        }
        st -= 1;
        arr[mod(st, arr.length)] = item;
        size += 1;
    }

    public void addLast(T item) {
        if (size == arr.length) {
            resize(arr.length * 2);
        }
        en += 1;
        arr[mod(en, arr.length)] = item;
        size += 1;
    }

    public int size() {
        return size;
    }

    public void printDeque() {
        System.out.println(toString());
    }

    private String toStringg() {
        StringBuilder sb = new StringBuilder();
        for (int i = st; i <= en; i++) {
            sb.append(arr[mod(i, arr.length)].toString() + " ");
        }
        return sb.toString();
    }

    public T removeFirst() {
        if (size == 0) {
            return null;
        }
        T current = arr[mod(st, arr.length)];
        arr[mod(st, arr.length)] = null;
        st += 1;
        size -= 1;
        if (size < arr.length / 4) resize(arr.length / 4);
        return current;
    }

    public T removeLast() {
        if (size == 0) {
            return null;
        }
        T current = arr[mod(en, arr.length)];
        arr[mod(en, arr.length)] = null;
        en -= 1;
        size -= 1;
        if (size < arr.length / 4) resize(arr.length / 4);
        return current;
    }

    public T get(int index) {
        if (index >= size) return null;
        return arr[mod(st + index, arr.length)];
    }

    public boolean equals(ArrayDeque<T> o) {
        return o.toStringg().equals(this.toStringg());
    }

    public Iterator<T> iterator() {
        return new ArrayDequeIterator<>(this);
    }

    private class ArrayDequeIterator<T> implements Iterator<T> {
        private ArrayDeque<T> arrayDeque;
        int ind = 0;

        public ArrayDequeIterator(ArrayDeque current) {
            this.arrayDeque = current;
        }

        @Override
        public boolean hasNext() {
            return ind + 1 < arrayDeque.size();
        }

        @Override
        public T next() {
            if (!hasNext()) return null;
            return arrayDeque.get(ind++);
        }
    }
}
