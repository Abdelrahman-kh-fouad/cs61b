package deque;

import java.util.Comparator;

public class MaxArrayDeque<T> extends ArrayDeque<T> {
    private Comparator<T> c;

    public MaxArrayDeque(Comparator<T> c) {
        this.c = c;
    }

    public T max(Comparator<T> cc) {
        T result = null;
        for (int i = 0; i < super.size(); i++) {
            int op = cc.compare(result, super.get(i));
            if (op == -1) result = super.get(i);
        }
        return result;
    }

    public T max() {
        return max(c);
    }
}
