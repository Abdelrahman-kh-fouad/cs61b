package timingtest;
import edu.princeton.cs.algs4.Stopwatch;

/**
 * Created by hug.
 */
public class TimeAList {
    private static void printTimingTable(AList<Integer> Ns, AList<Double> times, AList<Integer> opCounts) {
        System.out.printf("%12s %12s %12s %12s\n", "N", "time (s)", "# ops", "microsec/op");
        System.out.printf("------------------------------------------------------------\n");
        for (int i = 0; i < Ns.size(); i += 1) {
            int N = Ns.get(i);
            double time = times.get(i);
            int opCount = opCounts.get(i);
            double timePerOp = time / opCount * 1e6;
            System.out.printf("%12d %12.2f %12d %12.2f\n", N, time, opCount, timePerOp);
        }
    }

    public static void main(String[] args) {
        timeAListConstruction();
    }

    public static void timeAListConstruction() {
        AList<Integer>N, opCounts;
        AList<Double>times;
        N = new AList<Integer>();
        opCounts = new AList<Integer>();
        times = new AList<Double>();

        Stopwatch sw;
        for(int n = 0; n < 8; n+=1) {
            int all = 1000 * (1 << n);
            sw = new Stopwatch();
            AList<Integer>tmp = build(all);
            tmp.addLast(21);
            N.addLast(all);
            opCounts.addLast(all);
            double timeInSeconds = sw.elapsedTime();
            times.addLast(timeInSeconds);
        }
        printTimingTable(N, times, opCounts);
    }
    private static AList<Integer> build(int n) {
        AList result = new AList<>();
        for (int i = 0; i < n; i++) {
            result.addLast(i);
        }
        return result;
    }
}
