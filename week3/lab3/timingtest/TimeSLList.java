package timingtest;
import edu.princeton.cs.algs4.Stopwatch;

/**
 * Created by hug.
 */
public class TimeSLList {
    private static void printTimingTable(AList<Integer> Ns, AList<Double> times, AList<Integer> opCounts) {
        System.out.printf("%12s %12s %12s %12s\n", "N", "time (s)", "# ops", "microsec/op");
        System.out.printf("------------------------------------------------------------\n");
        for (int i = 0; i < Ns.size(); i += 1) {
            int N = Ns.get(i);
            double time = times.get(i);
            int opCount = opCounts.get(i);
            double timePerOp = time / opCount * 1e6;
            System.out.printf("%12d %12.2f %12d %12.2f\n", N, time, opCount, timePerOp);
        }
    }

    public static void main(String[] args) {
        timeGetLast();
    }

    public static void timeGetLast() {
        AList<Integer>N, opCounts;
        AList<Double>times;
        N = new AList<>();
        opCounts = new AList<>();
        times = new AList<>();
        int M = 10000;
        Stopwatch sw;
        for (int n = 1000; n <= 128000; n *=2) {
            SLList<Integer> tmp = build(n);
            sw = new Stopwatch();
            for (int i = 0; i < M; i++) {
                tmp.getLast();
            }
            double timeInSeconds = sw.elapsedTime();
            N.addLast(n);
            opCounts.addLast(M);
            times.addLast(timeInSeconds);
        }
        printTimingTable(N, times, opCounts);
    }
    private static SLList<Integer> build(int n) {
        SLList result = new SLList();
        for (int i = 0; i < n; i++) {
            result.addLast(i);
        }
        return result;
    }

}
